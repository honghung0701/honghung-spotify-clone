import React, { useEffect, useState } from "react";
import { FaHeadphones, FaHeart, FaRegClock, FaRegHeart } from "react-icons/fa";
import { Songs } from "./Songs";
import MusicPlayer from "./MusicPlayer";

export default function AudioList() {
  const [songs, setSongs] = useState(Songs);
  const [song, setSong] = useState(Songs[0].url);
  const [img, setImg] = useState(Songs[0].urlImg);

  useEffect(() => {
        const songs = document.querySelectorAll(".songs");

        function changeMenuActive() {
            songs.forEach(n => n.classList.remove("active"));
            this.classList.add("active"); 
        }
        songs.forEach(n=> n.addEventListener('click',changeMenuActive))
    }, [])

  const changeFavorite = (id) => {
    Songs.forEach((song) => {
      if (song.id == id) {
        song.favourite = !song.favourite;
      }
    });
    setSongs([...Songs]);
  };
  const setMainSong = (srcSong, srcImg) => {
    setSong(srcSong);
    setImg(srcImg);
  }
  return (
    <div className="audioList">
      <h2 className="title">
        The List <span>{Songs.length} songs</span>
      </h2>
      <div className="songsContainer">
        {Songs &&
          Songs.map((song, index) => {
            return (
              <div className="songs" 
              onClick={() => {
                setMainSong(song?.url, song?.urlImg)
              }}
              key={song?.id}>
                <div className="count">#{song.id}</div>
                <div className="song">
                  <div className="imgBox">
                    <img src={song?.urlImg} alt="" />
                  </div>
                  <div className="section">
                    <p className="songName">
                      {song?.name}
                      <span className="spanArtist">{song?.author}</span>
                    </p>
                    <div className="hits">
                      <p className="hit">
                        <i>
                          <FaHeadphones />
                        </i>
                        95,490,102
                      </p>
                      <p className="duration">
                        <i>
                          <FaRegClock />
                        </i>
                        03.04
                      </p>
                      <div
                        className="favourite"
                        onClick={() => {
                          changeFavorite(song?.id);
                        }}
                      >
                        {song?.favourite ? (
                          <i>
                            <FaHeart />
                          </i>
                        ) : (
                          <i>
                            <FaRegHeart />
                          </i>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
      </div>
      <MusicPlayer url={song} urlImg={img} />
    </div>
  );
}
