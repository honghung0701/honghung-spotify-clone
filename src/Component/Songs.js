export const Songs = [
  {
    name: "Sing me to sleep",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741533789224960/Alan_Walker_-_Sing_Me_To_SleepMP3_160K.mp3",
    id: 1,
    urlImg: "https://i.scdn.co/image/ab67616d0000b273a108e07c661f9fc54de9c43a",
    favourite: false,
  },
  {
    name: "Fade-NCS Release",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741536591806484/Alan_Walker_-_Fade_NCS_ReleaseMP3_160K.mp3",
    id: 2,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a108e07c661f9fc54de9c43a",
    favourite: true,
  },
  {
    name: "She-NCS Release",
    author: "Andromedik",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741544149549096/Andromedik_-_SHE_NCS_ReleaseMP3_160K.mp3",
    id: 3,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2737b8d8ca1a8e14506c8f35233",
    favourite: false,
  },
  {
    name: "About you-NCS Release",
    author: "Ascence",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741547203002389/Ascence_-_About_You_NCS_ReleaseMP3_160K.mp3",
    id: 4,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27335ca35166aba974dd2dd29a2",
    favourite: false,
  },
  {
    name: "On & On (feat. Daniel Levi) [NCS Release]",
    author: "Cartoon",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741549177995264/Cartoon_-_On___On_feat._Daniel_Levi_NCS_ReleaseMP3_160K.mp3",
    id: 5,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273d2aaf635815c265aa1ecdecc",
    favourite: false,
  },
  {
    name: "Castle [NCS Release]",
    author: "Clarx & Harddope",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741580619284540/Clarx___Harddope_-_Castle_NCS_ReleaseMP3_160K.mp3",
    id: 6,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273ba5db46f4b838ef6027e6f96",
    favourite: false,
  },
  {
    name: "Invincible [NCS Release]",
    author: "DEAF KEV ",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741667210035220/DEAF_KEV_-_Invincible_NCS_ReleaseMP3_160K.mp3",
    id: 7,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27362a57823eced1cb4fd93b2c1",
    favourite: false,
  },
  {
    name: " Blank [NCS Release]",
    author: "Disfigure",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741669588205598/Disfigure_-_Blank_NCS_ReleaseMP3_160K.mp3",
    id: 8,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27352b2a3824413eefe9e33817a",
    favourite: false,
  },
  {
    name: "Nekozilla [NCS Release]",
    author: "Different Heaven",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741669626085426/Different_Heaven_-_Nekozilla_NCS_ReleaseMP3_160K.mp3",
    id: 9,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27309065005b0fbfc4b320d05de",
    favourite: false,
  },
  {
    name: "Savannah (feat. Philly K) [NCS Release]",
    author: "Diviners",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741669626085426/Different_Heaven_-_Nekozilla_NCS_ReleaseMP3_160K.mp3",
    id: 10,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273b536cfb98c74558db48f8a46",
    favourite: false,
  },
  {
    name: "Cloud 9 [NCS Release]",
    author: "Itro & Tobu",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741836018974740/Itro___Tobu_-_Cloud_9_NCS_ReleaseMP3_160K.mp3",
    id: 11,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273d6fd719531afda5f9cc0e248",
    favourite: false,
  },
  {
    name: "Sky High [NCS Release]",
    author: "Elektronomia",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741836152668201/Elektronomia_-_Sky_High_NCS_ReleaseMP3_160K.mp3",
    id: 12,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27394ae8395433c0c7521ac25ba",
    favourite: false,
  },
  {
    name: "Where'd You Go (feat. Luna Lark) [NCS Release]",
    author: "Julius Dreisig",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741837642170382/Julius_Dreisig_-_Where_d_You_Go_feat._Luna_LarkMP3_160K.mp3",
    id: 13,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273100fb4649eae80922bf1acbf",
    favourite: false,
  },
  {
    name: "Island [NCS BEST OF]",
    author: "Jarico",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741836840665158/Jarico_-_Island_NCS_BEST_OFMP3_160K.mp3",
    id: 14,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273058c78826c35cbc3d03516c1",
    favourite: false,
  },
  {
    name: "Heroes Tonight (feat. Johnning) [NCS BEST OF]",
    author: "Janji",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741837385793556/Janji_-_Heroes_Tonight_feat._Johnning_NCS_ReleaMP3_160K.mp3",
    id: 15,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2739b07b787123fe99fffc9c789",
    favourite: false,
  },
  {
    name: "Landscape (Vlog No Copyright Music)",
    author: "Janji",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741839517024256/Jarico_-_Landscape_NCS_BEST_OFMP3_160K.mp3",
    id: 16,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27328007758bd0a5ffb1a07abeb",
    favourite: false,
  },
  {
    name: "Chasing Dreams [NCS Release]",
    author: "Jim Yosef & Valentina Franco",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741840136994846/Jim_Yosef___Valentina_Franco_-_Chasing_Dreams_NCSMP3_160K.mp3",
    id: 17,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273c14d955611b72addabd9163c",
    favourite: false,
  },
  {
    name: "Link [NCS Release]",
    author: "Jim Yosef",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741840224813096/Jim_Yosef_-_Link_NCS_ReleaseMP3_160K.mp3",
    id: 18,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2734f1608584777e92bddd5c904",
    favourite: false,
  },
  {
    name: "Symbolism [NCS Release]",
    author: "Electro-Light",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741841010065418/Electro-Light_-_Symbolism_NCS_ReleaseMP3_160K.mp3",
    id: 19,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273b44de5b9f5eba409dfa753e6",
    favourite: false,
  },
  {
    name: "Invisible [NCS Release]",
    author: "Julius Dreisig & Zeus X Crona",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741873582899230/Julius_Dreisig___Zeus_X_Crona_-_Invisible_NCS_RelMP3_160K.mp3",
    id: 20,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27391b709ced968e29e8f00dfe3",
    favourite: false,
  },
  {
    name: "Earth [NCS Release]",
    author: "K-391",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741894939901992/K-391_-_Earth_NCS_ReleaseMP3_160K.mp3",
    id: 21,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2732ea7097370b211b8213cdb5a",
    favourite: false,
  },
  {
    name: "Harpuia [NCS Release]",
    author: "Kadenza ",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741906344869928/Kadenza_-_Harpuia_NCS_ReleaseMP3_160K.mp3",
    id: 22,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273cac40eaa1c17e52e45a8098f",
    favourite: false,
  },
  {
    name: "Dreams [NCS Release]",
    author: "Lost Sky",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741919178653716/Lost_Sky_-_Dreams_NCS_ReleaseMP3_160K.mp3",
    id: 23,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273eb4d7af8109e201a4d438e02",
    favourite: false,
  },
  {
    name: "Fearless pt.II (feat. Chris Linton) [NCS Release]",
    author: "Lost Sky",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741949998399572/Lost_Sky_-_Fearless_pt.II_feat._Chris_Linton_NCMP3_160K.mp3",
    id: 24,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2733be40769b1c2361cea9f0843",
    favourite: false,
  },
  {
    name: "Ark [NCS Release]",
    author: "Ship Wrek & Zookeepers",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741988191338526/Ship_Wrek___Zookeepers_-_Ark_NCS_ReleaseMP3_160K.mp3",
    id: 25,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27303c03da08efb9051d8fae5d5",
    favourite: false,
  },
  {
    name: "Where We Started (feat. Jex) [NCS Release]",
    author: "Lost Sky",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741989177524264/Lost_Sky_-_Where_We_Started_feat._Jex_NCS_ReleaMP3_160K.mp3",
    id: 26,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a61f22690660bd5264495524",
    favourite: false,
  },
  {
    name: "Cradles [NCS Release]",
    author: "Sub Urban",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775741999978250250/Sub_Urban_-_Cradles_NCS_ReleaseMP3_160K.mp3",
    id: 27,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273cec7c8ebb684882dbaf476f5",
    favourite: false,
  },
  {
    name: "Feel Good [NCS Release]",
    author: "Syn Cole",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775742022085771278/Syn_Cole_-_Feel_Good_NCS_ReleaseMP3_160K.mp3",
    id: 28,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2736362397987db1fbf17f3d9b5",
    favourite: false,
  },
  {
    name: "Why Do I? (feat. Bri Tolani) [NCS",
    author: "Unknown Brain",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775742053337268224/Unknown_Brain_-_Why_Do_I__feat._Bri_Tolani_NCSMP3_160K.mp3",
    id: 29,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273c1d2f3c457d0ae59a7d7e4ce",
    favourite: false,
  },
  {
    name: "Infinite [NCS Release]",
    author: "Valence",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775742064893755402/Valence_-_Infinite_NCS_ReleaseMP3_160K.mp3",
    id: 30,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27361cb41224d1cc9dd052a5dba",
    favourite: false,
  },
  {
    name: "Mortals (feat. Laura Brehm) [NCS Release]",
    author: "Warriyo",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/775742084321509437/Warriyo_-_Mortals_feat._Laura_Brehm_NCS_ReleaseMP3_160K.mp3",
    id: 31,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273daf19986ce2c148768f5c362",
    favourite: false,
  },
  {
    name: "Alone",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782126454690938890/Alan_Walker_-_AloneMP3_128K.mp3",
    id: 32,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273153261ff7373a171c24ab9f9",
    favourite: false,
  },
  {
    name: "Look At Her Now",
    author: "Selena Gomez",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782128984070029322/Selena_Gomez_-_Look_At_Her_Now_Official_Music_VidMP3_128K.mp3",
    id: 33,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2732abcc266597eb46f897a8666",
    favourite: false,
  },
  {
    name: "I m Fakin",
    author: "Sabrina Carpenter",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129209711001610/Sabrina_Carpenter_-_I_m_Fakin_Audio_OnlyMP3_128K.mp3",
    id: 34,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273c2d8417d46ae0c823dc6463f",
    favourite: false,
  },
  {
    name: "Sing me to sleep",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129242318176266/Alan_Walker_-_Sing_Me_To_SleepMP3_128K.mp3",
    id: 35,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a108e07c661f9fc54de9c43a",
    favourite: false,
  },
  {
    name: "End of time",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129275322761236/K-391__Alan_Walker___Ahrix_-_End_of_Time_OfficialMP3_128K.mp3",
    id: 36,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27349b9fbf2345c02ca2387a357",
    favourite: false,
  },
  {
    name: "Faded",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129443250503720/Alan_Walker_-_FadedMP3_128K.mp3",
    id: 37,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a108e07c661f9fc54de9c43a",
    favourite: false,
  },
  {
    name: "The Spectre",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129468789358593/Alan_Walker_-_The_SpectreMP3_128K.mp3",
    id: 38,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a6a151ed88a170ae3a81eff5",
    favourite: false,
  },
  {
    name: "Freal Luv",
    author: "Marshmello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129488972480532/Far_East_Movement_x_Marshmello_-_Freal_Luv_ft._Chanyeol__Tinashe_Official_Video.m4a",
    id: 39,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273bc1684a7d68f275dd6db9939",
    favourite: false,
  },
  {
    name: "Let Me Love You",
    author: "DJ Snake ft Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129492180860928/DJ_Snake_-_Let_Me_Love_You_ft._Justin_Bieber_OffiMP3_128K.mp3",
    id: 40,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273212d776c31027c511f0ee3bc",
    favourite: false,
  },
  {
    name: "Let Me Love You",
    author: "DJ Snake ft Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129492180860928/DJ_Snake_-_Let_Me_Love_You_ft._Justin_Bieber_OffiMP3_128K.mp3",
    id: 41,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273212d776c31027c511f0ee3bc",
    favourite: false,
  },
  {
    name: "Here With Me Feat",
    author: "Marshmello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129612690948106/Marshmello_-_Here_With_Me_Feat._CHVRCHES_OfficialMP3_160K.mp3",
    id: 42,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273b91a6f7b2cda3f257e9ad571",
    favourite: false,
  },
  {
    name: "\u00c3\u2030chame La Culpa",
    author: "Luis Fonsi, Demi Lovato",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129660301279252/Luis_Fonsi__Demi_Lovato_-_Echame_La_Culpa_Video_OMP3_128K.mp3",
    id: 43,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2736be844b5d2fc0bf743fd7647",
    favourite: false,
  },
  {
    name: "Slow Down",
    author: "Selena Gomez",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129669578424330/Selena_Gomez_-_Slow_Down_OfficialMP3_128K.mp3",
    id: 44,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273da513d25e8b3cdd4f43f7512",
    favourite: false,
  },
  {
    name: "On my Way",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129671055605760/Alan_Walker__Sabrina_Carpenter___Farruko_-_On_MyMP3_128K.mp3",
    id: 45,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273d2aaf635815c265aa1ecdecc",
    favourite: false,
  },
  {
    name: "Summer",
    author: "Marshmello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129702650773544/Marshmello_-_Summer_Official_Music_Video_with_LeMP3_160K.mp3",
    id: 46,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a111c87c210cc9bff93948bd",
    favourite: false,
  },
  {
    name: "Can't Blame a Girl for Trying",
    author: "Sabrina Carpenter",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129754987692082/Sabrina_Carpenter_-_Can_t_Blame_a_Girl_for_TryingMP3_160K.mp3",
    id: 47,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2735d9a77afd5c144581be1e74e",
    favourite: false,
  },
  {
    name: "Baby",
    author: "Justin Bieber ft Ludacris",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129781373272084/Justin_Bieber_-_Baby_ft._Ludacris_Official_MusicMP3_128K.mp3",
    id: 48,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273629dc9e2e3bc20bbd7d92e4a",
    favourite: false,
  },
  {
    name: "Love You Like a Love Song",
    author: "Selena Gomez",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129797354618880/Selena_Gomez___The_Scene_-_Love_You_Like_A_Love_SoMP3_128K.mp3",
    id: 49,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2731c8193de8d62b2ffa49a09db",
    favourite: false,
  },
  {
    name: "We don't talk anymore",
    author: "Charlie Puth",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129797403770900/Charlie_Puth_-_We_Don_t_Talk_Anymore_feat._SelenaMP3_128K.mp3",
    id: 50,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273633a2d775747bccfbcb17a45",
    favourite: false,
  },
  {
    name: "Despacito",
    author: "Justin Bieber & Luis foni ",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129852181381121/Justin_Bieber_Despacito_Lyrics_--_ft._Luis_FonMP3_128K.mp3",
    id: 51,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273ef0d4234e1a645740f77d59c",
    favourite: false,
  },
  {
    name: "Yummy",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129879637950464/Justin_Bieber_-_Yummy_Official_VideoMP3_128K.mp3",
    id: 52,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2737fe4a82a08c4f0decbeddbc6",
    favourite: false,
  },
  {
    name: "PLAY",
    author: "Alan Walker ft Tungevaag",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129886810603530/Alan_Walker__K-391__Tungevaag__Mangoo_-_PLAY_AlanMP3_160K.mp3",
    id: 53,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2733899712512f50a8d9e01e951",
    favourite: false,
  },
  {
    name: "Girls Like You",
    author: "Maroon 5",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129896729214982/Maroon_5_-_Girls_Like_You_Lyrics_ft._Cardi_BMP3_128K.mp3",
    id: 54,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273deae7d931928fc1543e70203",
    favourite: false,
  },
  {
    name: "Alone pt II/2 ",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129915348385822/Alan_Walker___Ava_Max_-_Alone__Pt._IIMP3_128K.mp3",
    id: 55,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273c7870db6e559380bbc80fee8",
    favourite: false,
  },
  {
    name: "Mistletoe",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129951695962142/Justin_Bieber_-_Mistletoe_Official_Music_VideoMP3_160K.mp3",
    id: 56,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2730315efc555d5a157b0392652",
    favourite: false,
  },
  {
    name: "Me Necesita",
    author: "PRETTYMUCH, CNCO",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129959174012928/PRETTYMUCH__CNCO_-_Me_Necesita_Official_VideoMP3_160K.mp3",
    id: 57,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2736fd30af95d17ad6a8a3a74ad",
    favourite: false,
  },
  {
    name: "Senorita",
    author: "Shawn_Mendes,Camila_Cabello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782129973439102986/Shawn_Mendes__Camila_Cabello_-_Senorita_LyricsMP3_160K.mp3",
    id: 58,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273e6095c382c2853667c1623eb",
    favourite: false,
  },
  {
    name: "Ghost",
    author: "Au/Ra, Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130010088669204/Au_Ra__Alan_Walker_-_Ghost_Official_VideoMP3_160K.mp3",
    id: 59,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273e6f407c7f3a0ec98845e4431",
    favourite: false,
  },
  {
    name: "Sorry",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130017793998848/Justin_Bieber_-_Sorry_Official_Lyric_VideoMP3_160K.mp3",
    id: 60,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273f46b9d202509a8f7384b90de",
    favourite: false,
  },
  {
    name: "I'll Show You",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130047695585310/Justin_Bieber_-_I_ll_Show_You_Official_Music_VideMP3_160K.mp3",
    id: 61,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273f46b9d202509a8f7384b90de",
    favourite: false,
  },
  {
    name: "Alone",
    author: "Marshmello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130063050014732/Marshmello_-_Alone_Official_Music_VideoMP3_160K.mp3",
    id: 62,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273153261ff7373a171c24ab9f9",
    favourite: false,
  },
  {
    name: "Liar",
    author: "Camila Cabello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130071020371978/Camila_Cabello_-_Liar_LyricsMP3_160K.mp3",
    id: 63,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2732df02f0877da45295a759f4c",
    favourite: false,
  },
  {
    name: "Strongest",
    author: "Alan Walker ,Ina Wroldsen",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130079047221268/Alan_Walker___Ina_Wroldsen_-_Strongest_LyricsMP3_160K.mp3",
    id: 64,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2735510097a6f4875a4ad7c9095",
    favourite: false,
  },
  {
    name: "My Oh My",
    author: "Camila Cabello, DaBaby",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130082893398036/Camila_Cabello_-_My_Oh_My_Lyrics_ft._DaBabyMP3_160K.mp3",
    id: 65,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2735f53c0dbe5190a0af0fa28f3",
    favourite: false,
  },
  {
    name: "Believer",
    author: "Imagine Dragon",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130083555966986/Imagine_Dragons_-_Believer_LyricsMP3_160K.mp3",
    id: 66,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2735675e83f707f1d7271e5cf8a",
    favourite: false,
  },
  {
    name: "Heading Home",
    author: "Alan Walker ,Ruben",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130090208133120/Alan_Walker___Ruben_Heading_Home_Official_MusicMP3_160K.mp3",
    id: 67,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2735eab23d2260268d2fab870d0",
    favourite: false,
  },
  {
    name: "Company",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130102266757120/Justin_Bieber_-_Company_Official_Music_VideoMP3_160K.mp3",
    id: 68,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273f46b9d202509a8f7384b90de",
    favourite: false,
  },
  {
    name: "Sorry (PURPOSE : The Movement)",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130111000477756/Justin_Bieber_-_Sorry_PURPOSE___The_MovementMP3_160K.mp3",
    id: 69,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273f46b9d202509a8f7384b90de",
    favourite: false,
  },
  {
    name: "Bad Boys for Life",
    author: "Ritmo",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130127374385162/RITMO_Bad_Boys_For_Life_Remix___Official_MusiMP3_160K.mp3",
    id: 70,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27324147afe93bc68dfdb81209a",
    favourite: false,
  },
  {
    name: "Lost Control",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130131912228874/Alan_Walker_Lost_Control_Lyrics_ft._SoranaMP3_160K.mp3",
    id: 71,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273a108e07c661f9fc54de9c43a",
    favourite: false,
  },
  {
    name: "Closer",
    author: "The Chainsmokers",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130132722384907/The_Chainsmokers_-_Closer_Lyric_ft._HalseyMP3_128K.mp3",
    id: 72,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273495ce6da9aeb159e94eaa453",
    favourite: false,
  },
  {
    name: "Shameless",
    author: "Camila Cabello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130170789101578/Camila_Cabello_-_ShamelessMP3_160K.mp3",
    id: 73,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2735f53c0dbe5190a0af0fa28f3",
    favourite: false,
  },
  {
    name: "Unity",
    author: "Alan Walker",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130173805985792/Alan_x_Walkers_-_UnityMP3_160K.mp3",
    id: 74,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2734e14d839fbece313822fca82",
    favourite: false,
  },
  {
    name: "One Less Lonely Girl",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130199692443678/Justin_Bieber_-_One_Less_Lonely_Girl_Official_MusMP3_160K.mp3",
    id: 75,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2737c3bb9f74a98f60bdda6c9a7",
    favourite: false,
  },
  {
    name: "Together",
    author: "Marshmello",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130204519694336/Marshmello_-_Together_Official_Music_VideoMP3_160K.mp3",
    id: 76,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273ef5fa52beccb124c5f8da8d0",
    favourite: false,
  },
  {
    name: "Come & Get It",
    author: "Selena Gomez",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130215966605332/Selena_Gomez_-_Come___Get_ItMP3_128K.mp3",
    id: 77,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273cb16227d90152c2a5022bba1",
    favourite: false,
  },
  {
    name: "Boy With Luv",
    author: "BTS",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130258811289620/BTS____Boy_With_Luv_feat._HalMP3_160K.mp3",
    id: 78,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27318d0ed4f969b376893f9a38f",
    favourite: false,
  },
  {
    name: "Rockabye",
    author: "Clean Bandit",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130262342893618/Clean_Bandit_-_Rockabye_feat._Sean_Paul___Anne-MaMP3_160K.mp3",
    id: 79,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2731431c3bdf16aa99f71799d95",
    favourite: false,
  },
  {
    name: "Blank Space",
    author: "Taylor Swift",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130294697885696/Taylor_Swift_-_Blank_SpaceMP3_160K.mp3",
    id: 80,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27352b2a3824413eefe9e33817a",
    favourite: false,
  },
  {
    name: "What Do You Mean",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130340311203840/Justin_Bieber_-_What_Do_You_Mean__Official_MusicMP3_160K.mp3",
    id: 81,

    urlImg: "https://i.scdn.co/image/ab67616d0000b273f46b9d202509a8f7384b90de",
    favourite: false,
  },
  {
    name: "As Long As You Love Me",
    author: "Justin Bieber",
    url: "https://cdn.discordapp.com/attachments/836516032620920833/928662227601457242/As_Long_As_You_Love_Me_-_Backstreet_Boys_Lyrics_.mp3",
    id: 82,

    urlImg: "https://i.scdn.co/image/ab67616d0000b2736c20c4638a558132ba95bc39",
    favourite: false,
  },
  {
    name: "Confident",
    author: "Justin Bieber ft. Chance The Rapper",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130398385143818/Justin_Bieber_-_Confident_ft._Chance_The_Rapper_OMP3_160K.mp3",
    id: 83,

    urlImg: "https://i.scdn.co/image/ab67616d0000b27358ae8fddecbd2630005409c9",
    favourite: false,
  },
  {
    name: "Intentions",
    author: "Justin Bieber ft. Quavo",
    url: "https://cdn.discordapp.com/attachments/775740994595323954/782130437514067998/Justin_Bieber_-_Intentions_ft._Quavo_Official_VidMP3_160K.mp3",
    id: 84,
    urlImg: "https://i.scdn.co/image/ab67616d0000b27308e30ab6a058429303d75876",
    favourite: false,
  },
];
